<?php
namespace AccespermisBundle\Controller\Verification;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use AccespermisBundle\Resources\classe\Verification;
/* use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route; */

class VerifController extends Controller
{
    /**
     * @Route("/verification", name="verification")
     */
    public function indexAction()
    {
        $username = filter_input(INPUT_POST, 'username');
        $password = filter_input(INPUT_POST, 'pass');
        if (!isset($_SESSION))
        {
            session_start();
        }
        $_SESSION['username'] = $username;
        $_SESSION['pass'] = $password;
        $conn = new Verification();
        $conn->connexion();
        return $this->render('AccespermisBundle:Verification:verif.html.php');
    }
}