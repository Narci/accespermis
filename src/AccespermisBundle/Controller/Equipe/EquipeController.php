<?php

namespace AccespermisBundle\Controller\Equipe;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
/* use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route; */

class EquipeController extends Controller
{
    /**
     * @Route("/equipe", name="accueilEquipe")
     */
    public function indexAction()
    {
        return $this->render('AccespermisBundle:Equipe:equipe.html.php');
    }
}
