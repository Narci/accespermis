<?php

namespace AccespermisBundle\Controller\Photos;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
/* use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route; */

class PhotosController extends Controller
{
    /**
     * @Route("/photos", name="accueilPhotos")
     */
    public function indexAction()
    {
        return $this->render('AccespermisBundle:Photos:photos.html.php');
    }
}
