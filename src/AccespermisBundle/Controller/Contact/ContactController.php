<?php

namespace AccespermisBundle\Controller\Contact;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
/* use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route; */

class ContactController extends Controller
{
    /**
     * @Route("/contact", name="accueilContact")
     */
    public function indexAction()
    {
        return $this->render('AccespermisBundle:Contact:contact.html.php');
    }
}
