<?php

namespace AccespermisBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
/* use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route; */

class AccueilController extends Controller
{
    /**
     * @Route("/accueil", name="accueilDefault")
     */
    public function indexAction()
    {
        return $this->render('AccespermisBundle:Accueil:index.html.php');
    }
}