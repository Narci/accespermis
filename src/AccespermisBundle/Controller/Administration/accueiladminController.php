<?php

namespace AccespermisBundle\Controller\Administration;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use AccespermisBundle\Resources\classe\remove;
/* use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route; */

class accueiladminController extends Controller
{
    /**
     * @Route("/ACS_admin/accueiladmin", name="accueiladmin")
     */
    public function indexAction()
    {
        return $this->render('AccespermisBundle:Accueil:accueiladmin.html.php');
    }

    /**
     * @Route("/ACS_admin/listaccueil", name="listaccueil")
     */
    public function ListAction()
    {
        return $this->render('AccespermisBundle:Accueil:listaccueil.html.php');
    }

    /**
     * @Route("/ACS_admin/ajoutaccueil", name="ajoutaccueil")
     */
    public function AjoutAction()
    {
        return $this->render('AccespermisBundle:Accueil:ajoutaccueil.html.php');
    }

    /**
     * @Route("/ACS_admin/removeaccueil", name="removeaccueil")
     */
    public function RemoveAction()
    {
        return $this->render('AccespermisBundle:Accueil:removeaccueil.html.php');
    }

    /**
     * @Route("/ACS_admin/removeaccueil", name="remove")
     */
    public function Remove()
    {
        $remove = new remove();
        $remove->remove();
        return $this->render('AccespermisBundle:Accueil:removeaccueil.html.php');
    }
}
