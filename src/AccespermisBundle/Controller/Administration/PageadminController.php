<?php

namespace AccespermisBundle\Controller\Administration;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
/* use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route; */

class PageadminController extends Controller
{
    /**
     * @Route("/ACS_admin/dashboard", name="dashboard")
     */
    public function indexAction()
    {
        return $this->render('AccespermisBundle:Administration:administration.html.php');
    }
}
