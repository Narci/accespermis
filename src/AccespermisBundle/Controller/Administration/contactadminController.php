<?php

namespace AccespermisBundle\Controller\Administration;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
/* use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route; */

class contactadminController extends Controller
{
    /**
     * @Route("/ACS_admin/contactadmin", name="contactadmin")
     */
    public function indexAction()
    {
        return $this->render('AccespermisBundle:Administration:contactadmin.html.php');
    }
}
