<?php

namespace AccespermisBundle\Controller\Administration;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
/* use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;*/

class AdministrationController extends Controller
{
    /**
     * @Route("/ACS_admin")
     */
    public function indexAction()
    {
        return $this->render('AccespermisBundle:Administration:index.html.php');
    }
}
