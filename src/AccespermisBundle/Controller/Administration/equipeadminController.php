<?php

namespace AccespermisBundle\Controller\Administration;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
/* use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route; */

class equipeadminController extends Controller
{
    /**
     * @Route("/ACS_admin/accueilequipe", name="accueilequipe")
     */
    public function indexAction()
    {
        return $this->render('AccespermisBundle:Equipe:equipeadmin.html.php');
    }

    /**
     * @Route("/ACS_admin/listequipe", name="listequipe")
     */
    public function ListAction()
    {
        return $this->render('AccespermisBundle:Equipe:listequipe.html.php');
    }

    /**
     * @Route("/ACS_admin/ajoutequipe", name="ajoutequipe")
     */
    public function AjoutAction()
    {
        return $this->render('AccespermisBundle:Equipe:ajoutequipe.html.php');
    }

    /**
     * @Route("/ACS_admin/removeequipe", name="removeequipe")
     */
    public function RemoveAction()
    {
        return $this->render('AccespermisBundle:Equipe:removeequipe.html.php');
    }
}