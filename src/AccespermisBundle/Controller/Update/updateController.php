<?php
namespace AccespermisBundle\Controller\Update;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use AccespermisBundle\Resources\classe\update;
/* use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route; */

class updateController extends Controller
{   
    /**
     * @Route("ACS_admin/update", name="update")
     */
    public function updateACC($submit= "filter_input(INPUT_POST, 'btn-valid')")
    {
        $text = filter_input(INPUT_POST, 'texte');
        $_SESSION['text'] = $text;
        $conn = new update();
        $conn->updateacc();
        return $this->render('AccespermisBundle::update.html.php', [
            'submit' => $submit
        ]);
    }
}