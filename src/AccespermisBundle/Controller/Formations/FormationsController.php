<?php

namespace AccespermisBundle\Controller\Formations;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
/* use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route; */

class FormationsController extends Controller
{
    /**
     * @Route("/formations", name="accueilFormations")
     */
    public function indexAction()
    {
        return $this->render('AccespermisBundle:Formations:form.html.php');
    }
}
