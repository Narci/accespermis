<?php
namespace AccespermisBundle\Resources\classe;

use \PDO;
use AccespermisBundle\Controller\Verification\VerifController;

class Verification 
{
    public function __construct()
    {
        if (!isset($_SESSION))
        {
            session_start();
        }
    }

    function connexion()
    {
        $nomutil = 'accespermis';
        $motdepasse = 'OiQEGmk5BhNjOtmZ';
        $dsn = 'mysql:host=localhost;dbname=accespermis';
        $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        );

        try {
            $dbh = new PDO($dsn, $nomutil, $motdepasse, $options);
            $sth = $dbh->prepare('SELECT id, nomutil, motdepasse FROM utilisateurs WHERE nomutil = :user AND motdepasse = :pass');

            $sth->bindParam(':user', $_SESSION['username']);
            $sth->bindParam(':pass', $_SESSION['pass']);
            $sth->execute();

            $result = $sth->fetch();

            if ($result != false)
            {
                $_SESSION['connected'] = true;
                $_SESSION['id'] = $result['id'];
            }
            else 
            {
                session_destroy();
                session_unset();
                session_start();
                $_SESSION['connected'] = false;
            }
        }
        catch (PDOException $e)
        {
            print "Erreur :".$e->getMessage()."\n";
            $dbh = null;
        }
    }
}

?>