</head>
<body>

        <div class="container-fluide nav-haut fixed-top">
            <div class="container-fluide dash">
                <h1 class="text-center titre"> DASHBOARD </h1>
            </div>
        </div>
        <ul class="nav-verti">
            <li> <a href="dashboard"> DashBoard </a> </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Page </a>
                <div class="dropdown-menu" aria-labelledby="dropdown01" id="sous-menu" style="margin-left: 100%;">
                    <a class="dropdown-item lien-menu" href="accueiladmin"> Accueil </a>
                    <a class="dropdown-item lien-menu" href="#"> Formations </a>
                    <a class="dropdown-item lien-menu" href="#"> Coordonnees </a>
                    <a class="dropdown-item lien-menu" href="accueilequipe"> L'equipe </a>
                    <a class="dropdown-item lien-menu" href="#"> Photos </a>
                    <a class="dropdown-item lien-menu" href="contactadmin"> Contact </a>
                </div>
            </li>
            <li> <a href="compte"> Compte </a> </li>
            <li> <a href="help"> Aide </a> </li>
            <li> <a href="disconnect"> Deconnexion </a> </li>
        </ul>