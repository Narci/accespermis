<!-- Entête de la page d'administration -->
<?= $view->render('AccespermisBundle::header-admin.html.php'); ?>

<!-- Barre de navigation de la zone administration -->
<?= $view->render('AccespermisBundle::navbar-admin.html.php'); ?>  

    <!-- Container du texte de la page Accueil -->
      <?php 

      
      $nomutil = 'accespermis';
      $motdepasse = 'OiQEGmk5BhNjOtmZ';
      $dsn = 'mysql:host=localhost;dbname=accespermis';
      $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
      );
      try {
         $dbh = new PDO($dsn, $nomutil, $motdepasse, $options);
         $sth = $dbh->prepare('SELECT titre, texte FROM texte WHERE nom = "accueil";');
         $sth->execute();

         echo "<div class='alert alert-info info' style='width: 600px; margin-left: 20%; margin-top: 2%;'><b> INFOS : </b> Ici vous pouvez supprimer du contenu présent sur la page d'accueil. </div>";
         echo "<p style='margin-left: 20%; margin-top: 2%;'> <b> Contenu présent sur la page d'accueil : </b> </p>";
         while ($data = $sth->fetch(PDO::FETCH_ASSOC))
         {
             // Affichage du résultat
             echo "<div class='contenu'>";
                 echo "<h2 style='margin-left: 20%;'><b> Titre : </b>";
                     echo $data['titre'];
                 echo "</h2>";
                 echo "<textarea style='width: 900px; height: 200px; border: 1px solid black; margin-left: 20%'>";
                     echo $data['texte'];
                 echo "</textarea>";
            echo "<form method='post' action='remove'>";
                 echo "<input type='button' value='Supprimer' class='btn-primary'> </input>";
            echo "</form>";
                 echo "<hr />";
             echo "</div>";
         }
      }
      catch (PDOException $e)
      {
          print "Erreur :".$e->getMessage()."\n";
          $dbh = null;
      }
      ?> 

<!-- Footer de la page internet -->
<?= $view->render('AccespermisBundle::footer.html.php'); ?>