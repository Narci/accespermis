<!-- Entête de la page d'administration -->
<?= $view->render('AccespermisBundle::header-admin.html.php'); ?>

<!-- Barre de navigation de la zone administration -->
<?= $view->render('AccespermisBundle::navbar-admin.html.php'); ?>     

    <div style="margin-left: 20%; margin-top: 2%;">
        <h2> Gestion de la page Accueil </h2>
        <ul>
            <li><a href="listaccueil"> Lister </a></li>
            <li><a href="ajoutaccueil"> Ajouter </a></li>
            <li><a href="Modifier"> Modifier</a></li>
            <li><a href="removeaccueil"> Supprimer</a></li>
        </ul> 
    </div>    
    
<!-- Footer de la page internet -->
<?= $view->render('AccespermisBundle::footer.html.php'); ?>