<!-- Entête de la page d'administration -->
<?= $view->render('AccespermisBundle::header-admin.html.php'); ?>

<!-- Barre de navigation de la zone administration -->
<?= $view->render('AccespermisBundle::navbar-admin.html.php'); ?>     

    <?php

    /* Récupération de la saisie de l'utilisateur */
    $text = $_POST['textcontenu'];
    $titre = $_POST['titre'];

    /* Connexion à la base de données */
    $nomutil = 'accespermis';
    $motdepasse = 'OiQEGmk5BhNjOtmZ';
    $dsn = 'mysql:host=localhost;dbname=accespermis';
    $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
    );
    try {
    $dbh = new PDO($dsn, $nomutil, $motdepasse, $options);
    $sth = $dbh->prepare('INSERT INTO texte (nom, titre, texte) VALUES (:titre, :texte) WHERE nom = "accueil";');
    $sth->bindParam(":titre", $titre, PDO::PARAM_STR);
    $sth->bindParam(":texte", $texte, PDO::PARAM_STR);
    $sth->execute();
    
        echo "<form method='post'>";
            echo "<label for='titre'> <b> Titre :  </b> </label>";
            echo "<input type='text' id='titre' />";
            echo "<textarea name='textcontenu'>";
            echo "</textarea>";
        echo "</form>";
    }
    catch (PDOException $e)
    {
        print "Erreur :".$e->getMessage()."\n";
        $dbh = null;
    }
    ?>
    
<!-- Footer de la page internet -->
<?= $view->render('AccespermisBundle::footer.html.php'); ?>