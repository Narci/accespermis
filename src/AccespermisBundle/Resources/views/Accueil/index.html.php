<!-- Entête du site internet -->
<?= $view->render('AccespermisBundle::header.html.php'); ?>

        <!-- Fichier de style spécifique à la page d'accueil -->
        <link rel="stylesheet" href="<?= $view['assets']->getUrl('CSS/accueil.css') ?>" media="screen">

<!-- Navbar du site internet -->
<?= $view->render('AccespermisBundle::navbar.html.php'); ?>

    <!-- Image background de l'accueil du site internet -->
    <div class="container-fluid div-accueil">
      <img src="<?= $view['assets']->getUrl('image/vehicule.JPG') ?>" alt="ERROR_IMG_BACKGROUND" class="img-accueil">
      <img id="logowithtext" src="<?= $view['assets']->getUrl('image/LOGO_TEXTE_AVEC_CERCLE.gif') ?> ">
    </div>

    <div class="scrolling"> </div>

    <div class="container-text">
    <!-- Container du texte de la page Accueil -->
      <?php 
      $nomutil = 'accespermis';
      $motdepasse = 'OiQEGmk5BhNjOtmZ';
      $dsn = 'mysql:host=localhost;dbname=accespermis';
      $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
      );
      try {
         $dbh = new PDO($dsn, $nomutil, $motdepasse, $options);
         $sth = $dbh->prepare('SELECT titre, texte FROM texte WHERE nom = "accueil";');
         $sth->execute();

         while ($result = $sth->fetch(PDO::FETCH_ASSOC))
        {
            // Affichage du résultat
            echo "<div class='container text'>";
                echo "<h1 class='text-center'> <b>";
                    echo $result['titre'];
                echo "</b> </h1>";
                echo "<p>";
                    echo $result['texte'];
                echo "</p>";
                echo "<hr />";
            echo "</div>";
        }
      }
      catch (PDOException $e)
      {
          print "Erreur :".$e->getMessage()."\n";
          $dbh = null;
      }
      ?> 
      </div>

<!-- Footer de la page internet -->
<?= $view->render('AccespermisBundle::footer.html.php'); ?>
