<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" >
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title> Administration </title>
            <link rel="icon" type="image/x-icon" href="<?= $view['assets']->getUrl('favicon.ico') ?>" />
            <link rel="stylesheet" type="text/css" href="<?= $view['assets']->getUrl('CSS/navbar-admin.css') ?>" />
            <link rel="stylesheet" type="text/css" href="<?= $view['assets']->getUrl('CSS/bootstrap.min.css') ?>">