</head>
<body>
        <nav class="navbar navbar-expand-md navbar-perso navbar-dark fixed-top">
            <a class="navbar-brand" href="accueil"><img class="img-responsive logo-navbar" src="<?= $view['assets']->getUrl('image/logo_navbar.gif') ?>" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse navbar-toggle navbar-phone" id="navbarsExampleDefault">
            <ol class="nav navbar-nav ml-auto text-right ul-navbar">
            <li class="nav-item active nav-li"><a class="nav-link lien" href="accueil"> ACCUEIL </a></li>
            <li class="nav-item nav-li"><a class="nav-link lien" href="formations"> FORMATIONS </a></li>
            <li class="nav-item nav-li"><a class="nav-link lien" href="equipe"> L'EQUIPE </a></li>
            <li class="nav-item nav-li"><a class="nav-link lien" href="photos"> PHOTOS </a></li>
            <li class="nav-item nav-li"><a class="nav-link lien" href="contact"> CONTACTS </a></li>
            </ol>

        </div>
        </nav>