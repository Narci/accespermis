<?php
namespace AccespermisBundle\Resources\views;

/* Condition de vérification pour l'update de accueiladmin */
if (isset($submit) && $submit != "")
{

    ?>
        <!DOCTYPE html>
        <html lang="fr">
            <head>
                <title> Update </title>
                <meta http-equiv="refresh" content="2; URL=accueiladmin">
            </head>
            <body>
                <div style="margin-left: 40%; margin-top: 20%; border: 2px solid black; width: 500px; height: 100px; border-radius: 5px;">
                    <h1 style="color: green; font-size: 25px; font-family: sans-serif; text-align: center;"> Mise à jour effectuée avec succés !  </h1>
                </div>
            </body>
    <?php
}
else 
{
    ?>
    <!DOCTYPE html>
        <html lang="fr">
            <head>
                <title> Update </title>
                <meta http-equiv="refresh" content="2; URL=accueiladmin">
            </head>
            <body>
                <div style="margin-left: 40%; margin-top: 20%; border: 2px solid black; width: 400px; height: 100px; border-radius: 5px;">
                    <h1 style="color: red; font-size: 25px; font-family: sans-serif; text-align: center;"> La mise à jour n'à pas été effectuée. </h1>
                </div>
            </body>
    <?php
}
?>