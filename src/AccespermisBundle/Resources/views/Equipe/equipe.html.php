<!-- Entête du site internet -->
<?= $view->render('AccespermisBundle::header.html.php'); ?>

            <!-- Fichier de style spécifique à la page equipe -->
            <link rel="stylesheet" href="<?= $view['assets']->getUrl('CSS/equipe.css') ?>" media="screen">

<!-- Navbar du site internet -->
<?= $view->render('AccespermisBundle::navbar.html.php'); ?>

                <h3> Xavier et Frédéric vous accueillent pour la formation au Code, à la conduite Automobile et la formation Moto (Xavier) </h3>

                <div class="xavier"> </div>
                    <div class="txtxav">
                        <h2> Xavier </h2>
                        <hr>
                        <p> Formateur B / Moto depuis 2005. </p>
                    </div>
                </div>

                <div class="fred"> </div>
                    <div class="txtfred">
                        <h2> Fred </h2>
                        <hr>
                        <p> Formateur Code / B depuis 2015 - 2016. </p>
                    </div>
                </div>

<!-- Footer de la page internet -->
<?= $view->render('AccespermisBundle::footer.html.php'); ?>