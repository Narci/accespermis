<!-- Entête de la page d'administration -->
<?= $view->render('AccespermisBundle::header-admin.html.php'); ?>

<!-- Barre de navigation de la zone administration -->
<?= $view->render('AccespermisBundle::navbar-admin.html.php'); ?>     

    <!-- Menu de navigation pour la gestion de la page Equipe -->
    <div style="margin-left: 20%; margin-top: 2%;">
        <h2> Gestion de la page Equipe </h2>
        <ul>
            <li><a href="listequipe"> Lister </a></li>
            <li><a href="ajoutequipe"> Ajouter </a></li>
            <li><a href="Modifier"> Modifier</a></li>
            <li><a href="removeequipe"> Supprimer</a></li>
        </ul> 
    </div>    
    
<!-- Footer de la page internet -->
<?= $view->render('AccespermisBundle::footer.html.php'); ?>