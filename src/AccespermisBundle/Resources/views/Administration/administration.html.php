<!-- Entête de la page d'administration -->
<?= $view->render('AccespermisBundle::header-admin.html.php'); ?>
 
        <!-- fichier de style spécifique à la page DashBoard du service administration -->
        <link rel="stylesheet" type="text/css" href="<?= $view['assets']->getUrl('CSS/administration.css') ?>" />
        <script src="js/verification.js"></script>

<!-- Barre de navigation de la zone administration -->
<?= $view->render('AccespermisBundle::navbar-admin.html.php'); ?>

    <div class="container info">
        <h1 class="text-center"> PANNEL D'ADMINISTRATION ACCES PERMIS </h1>
        <p class="text-lg-left text "> <b> Bienvenue </b> sur ton panel d'administration Xavier, ici tu vas pouvoir modifier, supprimer, ajouter des éléments sur ton site internet. </p>
        <p class="text-lg-left"> Par exemple, tu vas pouvoir modifier les textes déjà présent sur ton site internet ou encore les supprimer mais encore en ajouter, si tu le souhaites. </p>
        <p class="text-lg-left"> J'ai également créé une page d'aide dans la barre de navigation de la zone d'administration, disponible <a href="aide"> ici. </a> </p>
        <p class="text-lg-left"> Sur cette page d'aide, tu trouveras les différentes explications concernant l'utilisation de l'interface graphique que j'ai mis en place. </p>
    </div>

<!-- Footer de la page internet -->
<?= $view->render('AccespermisBundle::footer.html.php'); ?>