<!DOCTYPE html>
<html lang="fr">
<head>
	<title> Administration </title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?= $view['assets']->getUrl('favicon.ico') ?>"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $view['assets']->getUrl('CSS/bootstrap.min.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $view['assets']->getUrl('animate/animate.css') ?>">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?= $view['assets']->getUrl('css-hamburgers/hamburgers.min.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $view['assets']->getUrl('animsition/css/animsition.min.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $view['assets']->getUrl('select2/select2.min.css') ?>">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?= $view['assets']->getUrl('daterangepicker/daterangepicker.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $view['assets']->getUrl('CSS/util.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= $view['assets']->getUrl('CSS/main.css') ?>">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?= $view['assets']->getUrl('image/vehicule.JPG') ?>')">
			<div class="wrap-login100 p-t-30 p-b-50">
				<span class="login100-form-title p-b-41">
					Connexion
				</span>
				<form class="login100-form validate-form p-b-33 p-t-5" method="post" action="verification">

					<div class="wrap-input100 validate-input" data-validate = "Entrez un nom d'utilisateur">
						<input class="input100" type="text" name="username" placeholder="Nom d'utilisateur">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Entrez un mot de passe">
						<input class="input100" type="password" name="pass" placeholder="Mot de passe">
						<span class="focus-input100"></span>
					</div>

					<div class="container-login100-form-btn m-t-32">
						<button class="login100-form-btn">
							Connexion
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?= $view['assets']->getUrl('jquery/jquery-3.2.1.min.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?= $view['assets']->getUrl('animsition/js/animsition.min.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?= $view['assets']->getUrl('bootstrap/js/popper.js') ?>"></script>
	<script src="<?= $view['assets']->getUrl('bootstrap/js/bootstrap.min.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?= $view['assets']->getUrl('select2/select2.min.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?= $view['assets']->getUrl('daterangepicker/moment.min.jss') ?>"></script>
	<script src="<?= $view['assets']->getUrl('daterangepicker/daterangepicker.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?= $view['assets']->getUrl('countdowntime/countdowntime.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?= $view['assets']->getUrl('js/main.js') ?>"></script>

</body>
</html>