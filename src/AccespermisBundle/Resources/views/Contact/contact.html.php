<!-- Entête du site internet -->
<?= $view->render('AccespermisBundle::header.html.php'); ?>

        <!-- Fichier de style spécifique à la page contact -->
        <link rel="stylesheet" href="<?= $view['assets']->getUrl('CSS/contact.css') ?>" media="screen">

<!-- Navbar du site internet -->
<?= $view->render('AccespermisBundle::navbar.html.php'); ?>

    <!-- Formulaire de contact -->
    <form action="" method="post" class="formulaire">
     <div class="alert alert-info"><b> INFOS : </b> Aucunes de vos informations ne seront sauvegardées </div>
     <div class="form-group">
        <label for="name"> Votre nom </label> 
        <input required type="text" name="name_req" class="form-control" id="name" placeholder="Entrez ici votre nom">
     </div>
     <div class="form-group">
        <label for="email"> Votre email </label>
        <input required type="email" name="email_req" class="form-control" id="email" placeholder="Entrez ici votre adresse email">
     </div>
     <div class="form-group">
        <label for="msg"> Entrez votre message </label>
        <textarea required name="msg_req" class="form-control" id="email" placeholder="Entrez ici votre message" ></textarea>
     </div>
     <button type="submit" class="btn btn-primary"> Envoyer </button>
   </form>
   <img src="<?= $view['assets']->getUrl('image/map_acs.png') ?>" alt="ERROR_MAP_ACS" class="map" >
   <form method ="post" class="horaire">
      <p name="adr"><b> 41 bis rue thiers, 59530 Le Quesnoy </b></p> 
      <p name="horaire"><b> Horaire : </b></p>
      <p name="contact"><b> Contactez nous : </b></p>
      <p name="tel"><b> 03 27 42 61 13 </b></p> 
      <p name="mail"><b> accespermis@free.fr </b></p>
      <a href="https://www.facebook.com/Acc%C3%A8s-Permis-413769712015794/"><img src="<?= $view['assets']->getUrl('image/facebook.png') ?>" title="N'hésite pas à nous rejoindre !" style="width: 30px; height: 30px;" /></a>
   </form>

<!-- Footer de la page internet -->
<?= $view->render('AccespermisBundle::footer.html.php'); ?>