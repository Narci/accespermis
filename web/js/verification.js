function checkWin()
{
    if (!myWindows) 
    {
        document.getElementById("msg").innerHTML = "'myWindow' has never been opened !";
    }
    else 
    {
        if (myWindow.closed)
        {
            document.getElementById("msg").innerHTML = "'myWindow' has been closed !";
        }
        else 
        {
            document.getElementById("msg").innerHTML = "'myWindow' has not been closed !";
        }
    }
}